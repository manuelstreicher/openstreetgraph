/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include <string>
#include <forward_list>
#include <unordered_set>

#include "../../../config.h"
#include "../../../constants.h"
#include "../../../raw_osm_network/raw_osm_network.h"

namespace tuk::osg::osmparser::expatparser
{
    enum ParserStatus {
        EMPTY_NETWORK,
        NETWORK_KNOWS_NODES_AND_WAYS,
        NETWORK_COMPLETE
    };

    class OsmParser {
        ParserStatus _status{EMPTY_NETWORK};
        OsmElement _open {NO_ELEMENT};
        id_type _currentStructureId {0};
        tag_map_t _currentTags;
        RestrictionType _restrictionType {NO_RESTRICTION};
        rawNetwork::RawNode * _currentNode {nullptr};
        std::list<id_type> _currentWayPoints;
        bool _isHighway {false};
        bool _isRestriction {false};
        std::unordered_map<id_type, std::forward_list<id_type>> _wayPoints;
        const std::unordered_set<std::string> & _highwayTypes;
        const std::list<std::array<std::string, 2>> & _kvNodePairs;

        rawNetwork::RawOsmNetwork & _network;
    public:
        explicit OsmParser(rawNetwork::RawOsmNetwork &network, const std::unordered_set<std::string> &highwayTypes,
                           const std::list<std::array<std::string, 2>> &kvNodePairs);

        ParserStatus getStatus() {return _status;}

        void setStatus(ParserStatus status);

        void endElementEmptyNetwork(const std::string &name);

        void startElementEmptyNetwork(const std::string &name, tag_map_t &attr);

        void startElementKnownNodesAndWays(const std::string &name, tag_map_t &attr);

        void endElementKnownNodesAndWays(const std::string &name);

    private:

        void openNode(tag_map_t &attr);

        void openWay(tag_map_t &attr);

        void openRelation(tag_map_t &attr);

        void addCurrentWay();

        void addCurrentNode(tag_map_t &attr);

        void addCurrentRestriction();

        void addTagToTagMap(tag_map_t &attr);

        void handleWayTag(tag_map_t &attr);

        void handleNodeTag(tag_map_t &attr);

        void clearTemporaries();

        void clearTemporariesWay();

        void clearTemporariesNode();

        void clearTemporariesRelation();

        void handleRestrictionTag(tag_map_t &attr);

        void setRestrictionType(const std::string &restriction);

        void handleRestrictionMember(tag_map_t &attr);

        void addRestrictionToMember(OsmElement memberType, id_type id, rawNetwork::RawRestriction &res);

        bool isNodeOrWayContainedInNetwork(tag_map_t &attr);

        static bool isNodeOrWay(tag_map_t &attr);

        void addWayPoint(id_type front);
    };
}
