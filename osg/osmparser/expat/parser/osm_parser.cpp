/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#include "osm_parser.h"

namespace tuk::osg::osmparser::expatparser {
    OsmParser::OsmParser(rawNetwork::RawOsmNetwork &network, const std::unordered_set<std::string> &highwayTypes,
                         const std::list<std::array<std::string, 2>> &kvNodePairs)
            : _network(network), _highwayTypes(highwayTypes), _kvNodePairs(kvNodePairs) {}

    void OsmParser::setStatus(ParserStatus status) {
        _status = status;
        if (_status == EMPTY_NETWORK) {
            std::cout << "Parsing osm ways corresponding to relevant streets..." << std::endl;
        } else if (_status == NETWORK_KNOWS_NODES_AND_WAYS) {
            std::cout << "Parsing crossing information and restrictions..." << std::endl;
        }

    }


    void OsmParser::startElementEmptyNetwork(const std::string &name, tag_map_t &attr) {
        // std::cout << "Starting Element " << name << " with id " << attr["id"] << std::endl;
        switch (_open) {
            case NO_ELEMENT:
                if (name == "way") openWay(attr);
                if (name == "relation") openRelation(attr);
                break;
            case NODE:
                break;
            case WAY:
                if (name == "nd") _currentWayPoints.push_front(std::stoull(attr["ref"]));
                if (name == "tag") handleWayTag(attr);
                break;
            case RELATION:
                if (name == "tag") handleRestrictionTag(attr);
                if (name == "member" && (!isNodeOrWay(attr) || !isNodeOrWayContainedInNetwork(attr)))
                    clearTemporariesRelation();
                break;
        }
    }

    void OsmParser::endElementEmptyNetwork(const std::string &name) {
        // std::cout << "Ending Element " << name << std::endl;
        switch (_open) {
            case NO_ELEMENT:
            case NODE:
                break;
            case WAY:
                if (name == "way") {
                    if (_isHighway) addCurrentWay();
                    clearTemporariesWay();
                }
                break;
            case RELATION:
                if (name == "relation") {
                    if (_isRestriction && _restrictionType != NO_RESTRICTION) addCurrentRestriction();
                    clearTemporariesRelation();
                }
                break;
        }

    }

    void OsmParser::startElementKnownNodesAndWays(const std::string &name, tag_map_t &attr) {
        switch (_open) {
            case (NO_ELEMENT):
                if (name == "node") openNode(attr);
                if (name == "relation") {
                    openRelation(attr);
                    if (!_network.containsRestriction(_currentStructureId)) clearTemporariesRelation();
                }
                break;
            case NODE:
                if (name == "tag") handleNodeTag(attr);
            case WAY:
                break;
            case RELATION:
                if (name == "member") handleRestrictionMember(attr);
                break;
        }
    }

    void OsmParser::endElementKnownNodesAndWays(const std::string &name) {
        switch (_open) {
            case (NO_ELEMENT):
                break;
            case NODE:
                if (name == "node") clearTemporariesNode();
            case WAY:
                break;
            case RELATION:
                if (name == "relation") clearTemporariesRelation();
                break;
        }
    }

    void OsmParser::openNode(tag_map_t &attr) {
        _currentStructureId = std::stoull(attr["id"]);
        auto nodePointer = _wayPoints.find(_currentStructureId);
        if (nodePointer != _wayPoints.end()) {
            _open = NODE;
            addCurrentNode(attr);
            for (auto & id : nodePointer->second) {
                rawNetwork::RawWay &way = _network.getWay(id);
                const std::list<id_type> &currentWaypoints = way.waypoints();
                bool isEndpoint = ((_currentStructureId == currentWaypoints.back()) ^ (_currentStructureId == currentWaypoints.front()));
                if (way.oneway()) {
                    _currentNode->addWay(((currentWaypoints.back() == _currentStructureId && isEndpoint) ? 0 : 1), true);
                } else {
                    _currentNode->addWay(isEndpoint ? 1 : 2, false);
                }

            }
        } else clearTemporariesNode();
    }

    void OsmParser::openWay(tag_map_t &attr) {
        _open = WAY;
        _currentStructureId = std::stoull(attr["id"]);
    }

    void OsmParser::openRelation(tag_map_t &attr) {
        _open = RELATION;
        _currentStructureId = std::stoull(attr["id"]);
    }

    void OsmParser::handleWayTag(tag_map_t &attr) {
        if (attr["k"] == "highway" && _highwayTypes.find(attr["v"]) != _highwayTypes.end()) _isHighway = true;
        addTagToTagMap(attr);
    }

    void OsmParser::addTagToTagMap(tag_map_t &attr) {
        _currentTags[attr["k"]] = attr["v"];
    }

    void OsmParser::addCurrentWay() {
        rawNetwork::RawWay &currentWay = _network.addWay(_currentStructureId);
        currentWay.setTags(std::move(_currentTags));
        bool wrongway = currentWay.setOnewayType();
        std::unordered_set<id_type> addedNodeids{};
        for (auto nd : _currentWayPoints) {
            //if (addedNodeids.find(nd) != addedNodeids.end()) continue;
            addWayPoint(nd);
            addedNodeids.insert(nd);
        }
        if (wrongway) {
            currentWay.addWaypoints(std::move(_currentWayPoints));
        } else {
            _currentWayPoints.reverse();
            currentWay.addWaypoints(std::move(_currentWayPoints));
        }
        _currentWayPoints.clear();
    }


    void OsmParser::addWayPoint(id_type front) {
        auto position = _wayPoints.insert(std::make_pair(front, std::forward_list<id_type>()));
        position.first->second.push_front(_currentStructureId);
    }

    void OsmParser::addCurrentRestriction() {
        rawNetwork::RawRestriction &currentRestriction = _network.addRestriction(_currentStructureId, _restrictionType);
        currentRestriction.setTags(std::move(_currentTags));
    }

    void OsmParser::clearTemporaries() {
        _open = NO_ELEMENT;
        _currentStructureId = 0;
    }

    void OsmParser::clearTemporariesWay() {
        clearTemporaries();
        _isHighway = false;
        _currentTags.clear();
        _currentWayPoints.clear();
    }

    void OsmParser::clearTemporariesNode() {
        clearTemporaries();
        _currentNode = nullptr;
    }

    void OsmParser::clearTemporariesRelation() {
        clearTemporaries();
        _isRestriction = false;
        _restrictionType = NO_RESTRICTION;
        _currentTags.clear();
    }

    void OsmParser::handleNodeTag(tag_map_t &attr) {
        _currentNode->addTag(attr["k"], std::move(attr["v"]));
    }

    void OsmParser::addCurrentNode(tag_map_t &attr) {
        coord_t lat = std::stod(attr["lat"]);
        coord_t lon = std::stod(attr["lon"]);
        _currentNode = &_network.addNode(_currentStructureId, lat, lon);
    }

    void OsmParser::handleRestrictionTag(tag_map_t &attr) {
        if (attr["k"] == "type" && attr["v"] == "restriction") _isRestriction = true;
        else if (attr["k"] == "restriction") setRestrictionType(attr["v"]);
        else addTagToTagMap(attr);
    }

    void OsmParser::handleRestrictionMember(tag_map_t &attr) {
        rawNetwork::RawRestriction &res = _network.getRestriction(_currentStructureId);
        OsmElement memberType = getElementType(attr["type"]);

        id_type id = std::stoull(attr["ref"]);
        addRestrictionToMember(memberType, id, res);
        if (attr["role"] == "from") res.add_from(id, memberType);
        else if (attr["role"] == "to") res.add_to(id, memberType);
        else if (attr["role"] == "via") res.add_via(id, memberType);
    }

    void
    OsmParser::addRestrictionToMember(OsmElement memberType, id_type id, rawNetwork::RawRestriction &res) {
        try {

            switch (memberType) {
                case NODE:
                    _network.getNode(id).addRestriction(res.id());
                    break;
                case WAY:
                    _network.getWay(id).addRestriction(res.id());
                    break;
                default:
                    std::cout << "Restriction " << res.id() << " has member " << memberType
                              << "  that is not a node or a way! (id: " << id << ")" << std::endl;
                    throw std::invalid_argument("Trying to add restriction to member that is not a node or a way!");
            }
        }
        catch (const std::out_of_range &error) {
            // If this is printed at any point, there is some error in the parser!!
            std::cout << "Restriction " << res.id() << " contains element of type " << memberType
                      << "  not in osm file! (id: " << id << ")" << std::endl;
        }
    }

    void OsmParser::setRestrictionType(const std::string &restriction) {
        _restrictionType = getRestrictionType(restriction);
        if (_restrictionType == NO_RESTRICTION) {
            std::cout << "Restriction " << restriction << " exists with Id " << _currentStructureId << std::endl;
            clearTemporariesRelation();
        }

    }

    bool OsmParser::isNodeOrWay(tag_map_t &attr) {
        enum OsmElement type = getElementType(attr["type"]);
        return type == NODE || type == WAY;
    }

    bool OsmParser::isNodeOrWayContainedInNetwork(tag_map_t &attr) {
        enum OsmElement type = getElementType(attr["type"]);
        id_type id = std::stoull(attr["ref"]);
        switch (type) {
            case NODE:
                return _wayPoints.find(id) != _wayPoints.end();
            case WAY:
                return _network.containsWay(id);
            default:
                throw std::invalid_argument("Element is not a node or a way!");
        }
    }

}
