/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/
#include <list>
#include <tuple>
#include <unordered_map>
#include <string>

#ifndef EXPAT_EXAMPLE_CONFIG_H
#define EXPAT_EXAMPLE_CONFIG_H

namespace tuk::osg {
    using id_type = long long unsigned int;
    using tag_map_t = std::unordered_map<std::string, std::string>;
    using length_t = double;
    using speed_t = unsigned int;
    using coord_t = double;
    using coord_pair_t = std::array<coord_t, 2>;
    using waypoints_t = std::list<coord_pair_t>;
}

#endif //EXPAT_EXAMPLE_CONFIG_H
