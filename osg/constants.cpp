/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#include "constants.h"

namespace tuk::osg
{
    OsmElement getElementType (const std::string &typeName)
    {
        if (typeName == "node") return NODE;
        if (typeName == "way") return WAY;
        if (typeName == "relation") return RELATION;
        std::cout << typeName << " is not an osm element!" << std::endl;
        return NO_ELEMENT;
    }

    enum RestrictionType getRestrictionType (const std::string &restriction)
    {
        if (restriction == "only_right_turn") return ONLY_RIGHT;
        if (restriction == "only_left_turn") return ONLY_LEFT;
        if (restriction == "only_straight_on") return ONLY_STRAIGHT;
        if (restriction == "no_left_turn") return NO_LEFT;
        if (restriction == "no_right_turn") return NO_RIGHT;
        if (restriction == "no_straight_on") return NO_STRAIGHT;
        if (restriction == "no_u_turn") return NO_UTURN;
        if (restriction == "no_entry") return NO_ENTRY;
        if (restriction == "no_exit") return NO_EXIT;
        if (restriction == "only_u_turn") return ONLY_UTURN;

        std::cout << "Restriction Type " << restriction << " not supported!" << std::endl;
        return NO_RESTRICTION;
    }

    enum HighwayType getHighwayType (const std::string &highway)
    {
        if (highway == "motorway") return MOTORWAY;
        if (highway == "trunk") return TRUNK;
        if (highway == "primary") return PRIMARY;
        if (highway == "secondary") return SECONDARY;
        if (highway == "tertiary") return TERTIARY;
        if (highway == "unclassified") return UNCLASSIFIED;
        if (highway == "residential") return RESIDENTIAL;
        if (highway == "motorway_link") return MOTORWAY_LINK;
        if (highway == "trunk_link") return TRUNK_LINK;
        if (highway == "primary_link") return PRIMARY_LINK;
        if (highway == "secondary_link") return SECONDARY_LINK;
        if (highway == "tertiary_link") return TERTIARY_LINK;
        if (highway == "living_street") return LIVING_STREET;
        if (highway == "service") return SERVICE;
        if (highway == "pedestrian") return PEDESTRIAN;
        if (highway == "track") return TRACK;
        if (highway == "bus_guideway") return BUS_GUIDEWAY;
        if (highway == "escape") return ESCAPE;
        if (highway == "raceway") return RACEWAY;
        if (highway == "road") return ROAD;
        if (highway == "footway") return FOOTWAY;
        if (highway == "bridleway") return BRIDLEWAY;
        if (highway == "steps") return STEPS;
        if (highway == "path") return PATH;
        if (highway == "cycleway") return CYCLEWAY;
        if (highway == "construction") return CONSTRUCTION;

        std::cout << "Highway Type " << highway << " not supported!" << std::endl;
        return NO_HIGHWAY;
    }

}

