/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include <fstream>
#include "../raw_osm_network/raw_osm_network.h"
#include "../config.h"
#include "node.h"
#include "street.h"
#include "os_graph_types.h"
#include "os_element.h"
#include "../../third_party/jsoncpp/json/json.h"

namespace tuk::osg::osgraph {
    class OSGraph {
    public:
        OSGraph();

        explicit OSGraph(rawNetwork::RawOsmNetwork &raw_network, std::list<std::array<std::string, 2>> &kvNodePairs);

        std::shared_ptr<OSNode> &add_osnode(rawNetwork::RawNode &rawNode);

        std::shared_ptr<OSStreet>
        add_osstreet(std::unordered_set<id_type> origins, OSNode &start, OSNode &end, waypoints_t &&waypoints,
                     tag_map_t attr, bool oneway, double length_meter);

        std::unordered_map<id_type, std::shared_ptr<OSNode>> &nodes() { return _osnodes; };

        void mergeDegreeTwoCrossings(std::unordered_set<std::string> & streetKeysForEqual);

        void only_keep_largest_component(std::list<std::list<std::weak_ptr<OSNode>>> &components);

        void remove_streets_outside_bounds(std::list<std::list<coord_pair_t>> &polygons);

        std::unordered_map<id_type, std::shared_ptr<OSStreet>> &streets() {return _osstreets;};

        bool containsEdge(OSNode &start, OSNode &end, std::shared_ptr<OSStreet> &street);

        // OUTPUT POSSIBILITIES
        int writeGraphToJson(const std::string &filename, const std::unordered_set<std::string> &streetKeys,
                             const std::unordered_set<std::string> &crossingKeys);
        int writeGraphToGeoJson(const std::string &filename, const std::unordered_set<std::string> &streetKeys,
                                const std::unordered_set<std::string> &crossingKeys);

        int number_of_nodes() {return _osnodes.size();};
        int number_of_streets() {return _osstreets.size();};
        int number_of_restrictions() {return _num_onlyRestrictions + _num_noRestrictions;};

    private:
        std::unordered_map<std::string, std::list<std::string>> _kvNodePairs;
        std::unordered_map<id_type, std::shared_ptr<OSNode>> _osnodes;
        std::unordered_map<id_type, std::shared_ptr<OSStreet>> _osstreets;
        std::forward_list<id_type> _deadStreetIds;

        id_type _nextStreetId{1};

        bool _logRestrictionProcess{false};

        std::list<std::list<id_type>> _onlyRestrictions;
        int _num_onlyRestrictions {0};
        std::list<std::list<id_type>> _noRestrictions;
        int _num_noRestrictions {0};

        std::list<std::shared_ptr<OSStreet>>
        _splitOsmWayIntoStreetparts(rawNetwork::RawWay &way, rawNetwork::RawOsmNetwork &network);

        bool _isEssential(rawNetwork::RawNode &node, rawNetwork::RawWay &way);
        static bool _sameTags(OSStreet &street1, OSStreet &street2, std::unordered_set<std::string> &streetKeysForEqual);
        void _remove_crossing(id_type crossingid);
        void _remove_crossing(OSNode & crossing);
        void _mark_incident_streets_dead(OSNode & node);
        void _remove_dead_streets();
        void _remove_dead_restrictions();
        bool _all_elements_of_restriction_in_graph(std::list<id_type>& res);

        void _handle_restrictions(rawNetwork::RawOsmNetwork &,
                                  way_streets_map_t<std::shared_ptr<OSStreet>>);

        bool _handle_restriction(rawNetwork::RawRestriction &restriction,
                                 way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                 rawNetwork::RawOsmNetwork &raw_network);

        bool _handle_no_restriction(rawNetwork::RawRestriction &restriction,
                                    way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                    rawNetwork::RawOsmNetwork &raw_network);

        bool _handle_only_restriction(rawNetwork::RawRestriction &restriction,
                                      way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                      rawNetwork::RawOsmNetwork &raw_network);

        bool _handle_no_entry_restriction(rawNetwork::RawRestriction &restriction,
                                          way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                          rawNetwork::RawOsmNetwork &network);

        bool
        _handle_no_exit_restriction(rawNetwork::RawRestriction &restriction,
                                    way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                    rawNetwork::RawOsmNetwork &network);

        bool
        _getOsgElementsInvolvedInRestriction(rawNetwork::RawRestriction &restriction,
                                             way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                             std::list<id_type> &involvedOsgElements);

        std::shared_ptr<OSStreet>
        _createNewStreet(std::unordered_set<id_type> origins, waypoints_t &&waypoints, rawNetwork::RawNode &start,
                         rawNetwork::RawNode &end, tag_map_t tags, bool oneway, double length_meter);


        static void _registerStreetInEndnodes(std::shared_ptr<OSStreet> &street_ptr);

        friend class OSStreet;

        friend class OSNode;

        bool _isFeasibleRestriction(rawNetwork::RawRestriction &restriction);

        bool _isFeasibleOnlyRestriction(rawNetwork::RawRestriction &restriction);

        bool _isFeasibleNoRestriction(rawNetwork::RawRestriction &restriction);

        bool _isFeasibleNoEntryRestriction(rawNetwork::RawRestriction &restriction);

        bool _isFeasibleNoExitRestriction(rawNetwork::RawRestriction &restriction);

        void _fillInStreetsAndCrossings(Json::Value &root, const std::unordered_set<std::string> &streetKeys,
                                        const std::unordered_set<std::string> &crossingKeys);

        int _write(Json::Value &root, const std::string &filename);

        void _fillInStreet(Json::Value &streets, OSStreet &street, const std::string &streetId,
                           const std::unordered_set<std::string> &streetKeys);

        void _fillInCrossing(Json::Value &nodes, OSNode &node, const std::string &nodeid,
                             const std::unordered_set<std::string> &crossingKeys);

        void _appendNode(std::shared_ptr<OSNode> &node, Json::Value &features,
                         const std::unordered_set<std::string> &crossingKeys);

        void _appendStreet(std::shared_ptr<OSStreet> &street, Json::Value &features, std::array<coord_t, 4> &bbox,
                           const std::unordered_set<std::string> &streetKeys);

        void _appendCoord(Json::Value &coord, const std::array<coord_t, 2> &wp, std::array<coord_t, 4> &bbox);

        void _appendRestrictions(std::list<id_type> &res, Json::Value &features, const std::string &resType, int count);

        void _addStreetsAndCrossingsAsGeo(Json::Value &features, std::array<coord_t, 4> &bbox,
                                          const std::unordered_set<std::string> &streetKeys,
                                          const std::unordered_set<std::string> &crossingKeys);

        static OSNode *
        _getIntersectionNode(std::list<std::shared_ptr<OSStreet>> &streets_1,
                             std::list<std::shared_ptr<OSStreet>> &streets_2);

        bool _isFeasibleRestrictionWithViaNode(rawNetwork::RawRestriction &restriction,
                                               way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets);

        bool _getOsgElementsInvolvedInRestrictionViaIsNode(rawNetwork::RawRestriction &restriction,
                                                           way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                                           std::list<id_type> &involvedOsgElements);

        bool _getOsgElementsInvolvedInRestrictionViaIsWay(rawNetwork::RawRestriction &restriction,
                                                          way_streets_map_t<std::shared_ptr<OSStreet>> &wayToStreets,
                                                          std::list<id_type> &involvedOsgElements);

        std::list<std::shared_ptr<OSStreet>>
        _getIncidentStreetsFromList(std::list<std::shared_ptr<OSStreet>> &streets, id_type nodeid);

        void _pushBackStreetAndNodeIds(std::list<id_type> &elements, std::list<std::shared_ptr<OSStreet>> &streets,
                                       id_type endnodeid);

        void _addStreetNodeStreetRestrictionToList(std::list<id_type> &elements, std::shared_ptr<OSStreet> &inStreet,
                                                   id_type crossingid,
                                                   std::shared_ptr<OSStreet> &outStreet);

        void _fillInRestrictions(Json::Value &root);

        std::shared_ptr<OSStreet> _resolveDegreeTwoNode(OSNode &node, OSStreet &street1, OSStreet &street2);

        std::shared_ptr<OSStreet>
        _createNewStreet(std::unordered_set<id_type> origins, waypoints_t &&waypoints, OSNode &start, OSNode &end,
                         tag_map_t tags, bool oneway, double length_meter);

        bool nodeContainedInPolys(std::list<std::list<coord_pair_t>> &polygons, OSNode &node);
    };
}
