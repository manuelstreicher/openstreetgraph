/*
* Licensed under the MIT license:
*
* Copyright (c) 2020 Manuel Streicher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*
*/

#pragma once

#include <iostream>
#include <utility>
#include <string>
#include <memory>
#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "essentials/raw_node.h"
#include "essentials/raw_way.h"
#include "essentials/raw_restriction.h"

namespace tuk::osg::rawNetwork
{
    class RawOsmNetwork {

    public:
        RawOsmNetwork();

        RawWay & addWay(id_type id);
        RawNode & addNode(id_type id, coord_t lat, coord_t lon);
        RawRestriction & addRestriction(id_type id, RestrictionType type);

        RawRestriction & getRestriction(id_type id);
        RawWay & getWay(id_type id);
        RawNode & getNode(id_type id);

        bool containsRestriction(id_type id);
        bool containsWay(id_type id);
        bool containsNode(id_type id);

        int number_nodes() const;
        int number_ways() const;
        int number_restrictions() const;
        
        std::unordered_map<id_type, RawWay>& ways()
        {
          return _ways;
        }
        std::unordered_map<id_type, RawNode>& nodes() 
        {
          return _nodes;
        }
        const std::unordered_map<id_type, RawRestriction>& restrictions() const
        {
          return _restrictions;
        }

    private:
        std::unordered_map<id_type, RawWay> _ways;
        std::unordered_map<id_type, RawNode> _nodes;
        std::unordered_map<id_type, RawRestriction> _restrictions;
    };
}
